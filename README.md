# IoT Stack (influxDB, mosquitto, nodeRED, Grafana) <a name="overview"></a>

## Getting started 
This tutorial shows how a typical IoT Stack can be deployed on a container orchestration using Docker and Docker Compose. 

If you need to install a container orchestration first, you can follow this Tutorial: [https://gitlab.com/fcschmid/container-orc-setup](https://gitlab.com/fcschmid/container-orc-setup)

Services for a typical IoT scenario for the evaluation of sensor data are implemented. 
For this purpose, the following open source services are used:

Database technology: influxDB

message broker: mosquitto

browser-based sequence editor: Nodered

visualization: Grafana


### Annotation:
Hostname: datalog
USER: pi

## Step 1:
We create a new folder for containers under the user:

    sudo mkdir containers

We change to the directory

    cd containers

We create a directory with subfolder for the IoTStack of the dataenlogger

    sudo mkdir -p ./datalog/{grafana,influx,mosquitto,nodered}

We change to the directory

    cd datalog

We assign new permissions to each created subfolder:

    sudo chown 1001:1001 grafana/
    sudo chown 1001:1001 influx/
    sudo chown 1001:1001 mosquitto/
    sudo chown 1001:1001 nodered/

## Step 2: 
Each container now has its own folder. There we create a file and name it dockerfile.
Dockerfile contains instructions relevant to building the container, mainly the version of the service described. In the simplest case, the file thus contains only the reference to the base image in the Docker Registry.
Later, the files have the advantage that we can directly build configurations etc. into the container.

We change to the folder for Grafana

    cd grafana

We create the file with:

    sudo nano dockerfile

We write the following in the file:

    FROM grafana/grafana:latest

Then just save with CTRL + O and CTRL + X and exit.

We change into the folder for InfluxDB

    cd ../
    cd influx

We create the file with:

    sudo nano dockerfile

We write the following in the file:

    FROM influxdb:1.8

Then just save with CTRL + X and Y and ENTER.

We change to the folder for Mosquitto

    cd ../
    cd mosquitto

We create the file with:

    sudo nano dockerfile

We write the following in the file:

    FROM eclipse-mosquitto:1.6.14

Then just save with CTRL + X and Y and ENTER.

We change to the folder for NodeRED

    cd ../
    cd nodered

We create the file with:

    sudo nano dockerfile

We write the following in the file:

    FROM nodered/node-red:latest

Then just save with CTRL + O and CTRL + X and exit.
We change to the folder above

    cd ../
    
The following links show the documentation of the services
Grafana: [https://hub.docker.com/r/grafana/grafana](https://hub.docker.com/r/grafana/grafana)
NodeRed: [https://hub.docker.com/r/nodered/node-red](https://hub.docker.com/r/nodered/node-red)
MQTT Broker: [https://hub.docker.com/_/eclipse-mosquitto](https://hub.docker.com/_/eclipse-mosquitto)
InfluxDB: [https://hub.docker.com/_/influxdb](https://hub.docker.com/_/influxdb)

## Step 3: 
We now create the Docker Compose file. It declares each service and the network configuration for the Docker network. 
This will start the containers and connect them to each other. It also declares the deployment to the outside world to use them as services.
To ensure that the settings are persistent, volumes are declared accordingly.
The ports part specifies which ports are mapped from the container.

We create the file:

    sudo nano docker-compose.yml

First we declare the version of docker-compose.yml

    version: "3"

We create a section for the services:

    services:

Under this, each service is described. In yml files it is important to pay attention to the spaces and declaration characters. We put two spaces in the line below the "services:" and declare NodeRED as the first service:

    version: "3"
    services:
      nodered: 
        build: ./nodered
        image: localbuild/nodered:april2021
        container_name: nodered
        restart: unless-stopped
        ports: 
          - "1880:1880"
        volumes:
          - noderedData:/data
        networks:
          - edge

Below "nodered" comes the reference to the subfolder with the instructions in the dockerfile file. The naming of the image and the container name. Below that is declared what the start-stop behavior of the container is. The port mapping, volumes and network settings are declared.

In the same way we describe the other services:

    grafana:
      build: ./grafana
      image: localbuild/grafana:april2021
      container_name: grafana
      restart: unless-stopped
      ports:
        - "3000:3000"
      volumes: 
        - grafanaData:/var/lib/grafana
      networks:
        - edge


    influxdb:
      build: ./influx
      image: localbuild/influxdb:april2021
      container_name: influxdb
      restart: unless-stopped
      environment:
        INFLUXDB_DB: DBONE
        INFLUXDB_HTTP_AUTH_ENABLED: "true
        INFLUXDB_ADMIN_USER: change
        INFLUXDB_ADMIN_PASSWORD: this
        INFLUXDB_USER: and_change
        INFLUXDB_USER_PASSWORD: this_too
      ports:
        - "8082:8082"
        - "8086:8086"
        - "8089:8089"
      volumes:
        - influxData:/var/lib/influxdb
      networks: 
        - edge


    mosquitto:
      build: ./mosquitto
      image: localbuild/mosquitto:april2021
      container_name: mosquitto
      restart: unless-stopped
      ports: 
        - "8883:1883"
        - "9001:9001"
      volumes: 
        - mosquittoLog:/mosquitto/log
        - mosquittoData:/mosquitto/data
      networks:
        - edge


Next we declare network settings
      
      
    networks:
      edge:

Last, we declare the perstistent volumes:
    volumes:
      grafanaData:
      noderedData:
      influxData:
      mosquittoLog:
      mosquittoData:
  

The complete file looks as follows:

    version: "3"
    services:
      nodered: 
        build: ./nodered
        image: localbuild/nodered:april2021
        container_name: nodered
        restart: unless-stopped
        ports: 
          - "1880:1880"
        volumes:
          - noderedData:/data
        networks:
          - edge
      grafana:
        build: ./grafana
        image: localbuild/grafana:april2021
        container_name: grafana
        restart: unless-stopped
        ports:
          - "3000:3000"
        volumes: 
          - grafanaData:/var/lib/grafana
        networks:
          - edge
      influxdb:
        build: ./influx
        image: localbuild/influxdb:april2021
        container_name: influxdb
        restart: unless-stopped
        environment:
          INFLUXDB_DB: DBONE
          INFLUXDB_HTTP_AUTH_ENABLED: "true"
          INFLUXDB_ADMIN_USER: change
          INFLUXDB_ADMIN_PASSWORD: this
          INFLUXDB_USER: and_change
          INFLUXDB_USER_PASSWORD: this_too
        ports:
          - "8082:8082"
          - "8086:8086"
          - "8089:8089"
        volumes:
          - influxData:/var/lib/influxdb
        networks: 
          - edge
      mosquitto:
        build: ./mosquitto
        image: localbuild/mosquitto:april2021
        container_name: mosquitto
        restart: unless-stopped
        ports: 
          - "8883:1883"
          - "9001:9001"
        volumes: 
          - mosquittoLog:/mosquitto/log
          - mosquittoData:/mosquitto/data
        networks:
          - edge

    networks:
      edge:

    volumes:
      grafanaData:
      noderedData:
      influxData:
      mosquittoLog:
      mosquittoData:

  



## Step 4: 
Now we are ready to start the stack.
We start the creation of the containers with the following command:

    sudo docker-compose up -d 

Now we test the whole thing: 

    docker ps

Then we navigate in a browser to

    datalog:1880 -> nodered

then in a new tab to: 

    datalog:3000 -> grafana

To terminate the stack we enter the following in the kmando line:

    docker compose down

To also delete the persistent volumes when we no longer need them, we enter the following in the kmando line:

    docker volume prune 

To delete the images, we enter the following in the Kmando line:

    docker image prune 

To detach the network settings, we enter the following in the kmando line:

    docker compose build network prune


## Step 5: Prepare SenseHAT 
Then, we perform an update and upgrade so that the Pi has updated libraries and software versions:

    sudo apt-get update && sudo apt-get upgrade -y

Reboot

    sudo reboot

Log in and make a new folder

    mkdir sensehat

Install the sensehat Library

    sudo apt-get install sense-hat

Reboot

    sudo reboot
    
Change the directory and create first Python scripts

    cd sensehat

    sudo nano sense_thp.py

A script example could look lilke this

    # Program for measuring temperature and humidity and pressure with Sense Hat
    from sense_hat import SenseHat
    
    sense = SenseHat()
    sense.clear()
    
    temp = sense.get_temperature()
    humidity = sense.get_humidity()
    pressure = sense.get_pressure()
    print('t:{:.2f} rH:{:.0f} p:{:.0f}' .format(temp, humidity, pressure))    

Execute the python script:

    python3 sense_thp.py

Some sensors need calibration. Install the necessary software and run the calibration program as described at[rasoberrypi.com](https://www.raspberrypi.com/documentation/accessories/sense-hat.html#hardware-calibration)


## Step 6: Integrate SenseHAT into Container network

Next we will integrate an additional container to our stack enabling communication with the sensehat.
Change the directory and make a new folder.

    cd containers/datalog

    sudo mkdir control-sense-hat

    sudo chown 1001:1001 control-sense-hat/





